package com.example.asus.memegenerator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button gen;
    EditText top;
    EditText bot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gen=(Button)findViewById(R.id.butt);

        bot = (EditText)findViewById(R.id.inputbot);
        top = (EditText) findViewById(R.id.inputTop);
        final String s, d;

        gen.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent second = new Intent(MainActivity.this,image.class);
                        String s = top.getText().toString();
                        String d = bot.getText().toString();
                        second.putExtra("top",s);
                        second.putExtra("bot",d);
                        startActivity(second);
                    }
                }

        );
    }
}
